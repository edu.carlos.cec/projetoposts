const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const db = require('./src/configs/sequelize')


app.use(bodyParser.json())

app.use(bodyParser.urlencoded({extended: true}))

//Explicar que estamos usando uma pasta estática 
app.use(express.static('public'))

db.sequelize.sync({alter : true}).then(() => {
    console.log("Deu certo a criação do banco  (DROP E/OU CREATE)")
})


require('./src/user/routes')(app)
require('./src/post/routes')(app)

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/public/views/index.html")
})

var server = app.listen(3000, () => {
    console.log("Server rodando na porta " + server.address.port + " no host " + server.address().adress)
})