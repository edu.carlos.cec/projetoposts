module.exports = (app) => {
    const controller = require('./controller')
    
    //Criando um novo usuário
    app.post('/usuarios', controller.create)


    //Busca todos os usuários
    app.get('/usuarios', controller.findAll)
}