module.exports = (app) => {
    const controller = require('./controller')
    
    //Criando um novo post
    app.post('/post', controller.create)

    //Busca todos os posts
    app.get('/post', controller.findAll)

    //Atualiza um post por id
    app.put('/post', controller.update)

    //Remove post por id    método
    app.delete('/post', controller.remove)

    //Mostrar post por id
    app.get('/posts/search', controller.search);

    //Mostrar post por usuarios
    app.get('/posts/users', controller.FindUsers);

    //Cadastrar usuarios
    app.post('/posts/cadastro', controller.createUser)

    //Mostrar post por usuarios
    app.get('/posts/usuarioId', controller.acharIdUsuario);
}