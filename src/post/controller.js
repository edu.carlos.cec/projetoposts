const db = require("./../configs/sequelize")
const Post = require("./model")
const User = require("./../user/model")
const { username } = require("../configs/data-base")
const  { Op } = require ("sequelize")


exports.create = (req, res) => {
    Post.create({
        content: req.body.content,
        user:{
            firstname: req.body.firstname,
            lastname: req.body.lastname,
        }, include:[
            {
                association: Post.User,
                include: [User.Post]
            }
        ]
    }).then((post)=>{
        console.log(post)
        res.send(post)
    }).catch((err)=>{
        console.log("\n=======================\n")
        console.log(req.body.firstname)
        console.log(req.body.lastname)
        console.log(req.body.content)
        console.log("\n=======================\n")
        console.log("Erro...: " + err)
    })
}


exports.findAll = (req, res) => {
    //Ordena a coluna com o order
    Post.findAll({include : User, order : ['createdAt']}).then(posts => {
        //Envia um json com todos os posts
        res.send(posts)
    })
}

//Fazer o update do comentario
exports.update = (req, res) => {
    Post.update({
        content: req.body.content
    }, 
    {
        where : {
            id:req.body.id
        }
    }).then(() => {
        res.send({'message' : "ok"})
    })
}

//Fazer o remmove do comentario
exports.remove = (req, res) => {
    Post.destroy({
        where : {
            id: req.body.id
        }
    }).then((affectedRows) => {
        res.send({'message': 'Ok', 'affectedRows' : affectedRows})
    })
    
}

exports.search  = (req,res)  =>  {
    console.log(req.query.content)
    //console.log(req.body)
    //console.log(req.query)
    Post.findAll ({

        include : User,
        where:{
        content : db.sequelize.where(
                db.Sequelize.fn('upper' ,db.sequelize.col('content')),
                {
                    [Op.like] : '%' + req.query.content.toUpperCase() + '%'
                }
            )
        }
    }).then(data =>  {
      res.send(data)
    }).catch(err  =>  {
      console.log('Erro:'+err)
    }) ;
    
}

exports.FindUsers = (req, res) => {
    User.findAll({
        attributes: ["firstname"]
    }).then(data => {
        res.send(data)
    }).catch(err => {
        console.log('Erro '+ err)
    })
}

exports.acharIdUsuario = (req, res) => {
    User.findAll({
        attributes: ["id"],
        where:{
            firstname: req.body.firstname
        }
    }).then(data => {
        res.send(data)
    }).catch(err => {
        console.log(data)
        console.log("Erro "+err)
    })
}


exports.createUser = (req, res) => {
    User.create({
        firstname: req.body.firstname,
        lastname: req.body.lastname
    }).then((user)=>{
        //console.log(user)
        res.send(user)
    }).catch((err)=>{
        console.log("\n=======================\n")
        console.log(req.body.firstname)
        console.log(req.body.lastname)
        console.log(req.body.content)
        console.log("\n=======================\n")
        console.log("Erro...: " + err)
    })
}

//eager --> carrega tudo (posts + relacionamentos) -- mais pesado
//lazy --> carrega apenas a entidade prinicipal e os demais quando for necessário -- Busca mais leve
