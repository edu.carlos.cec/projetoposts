
Posts = {
    add : () => {
        var t = {};
        t.content = $("#content").val()
        t.firstname = $("#usuarios").val()
        t.lastname = " "
        //t.idUsuario = Posts.acharIdUsuario();

        $.ajax({
            type: 'POST',
            url:  '/post',
            data: t,
            dataType: 'json',
            success: Posts.template
        })
        return false
    },

    template : function(data)  {

        var comment = 
        $('<div></div>')
        .attr('id', 'comment-'+data.id)
        .attr('class', 'comment')


        var content = $('<textarea></textarea>')
        .attr('class', 'content')
        .attr('disabled', true)
        .html(data.content)

        //
        var user = $("<p></p>")
        .attr('class', 'user')
        .html("Por << ")

        console.log("Usuario..: " + data.user)

        var dtCreation = new Date(data.createdAt)
        dtCreation = (dtCreation.getDate() < 10 ? "0" + dtCreation.getDate() : dtCreation.getDate())
                     + "/" + ((dtCreation.getMonth() + 1) < 10 ? "0" + (dtCreation.getMonth() + 1) : (dtCreation.getMonth() + 1))
                     + "/" + dtCreation.getFullYear()

        var date = $('<span></span>').attr('class', 'date').html(dtCreation)


        var btnEdit = $('<button></button>').attr('class', 'edit').html("Editar")
        var btnSave = $('<button></button>').attr('class', 'save hidden').html("Salvar")
        var btnRemove = $('<button></button>').attr('class', 'remove').html("Remover")
        

        //Construir um evento
        $(btnEdit).on('click', (event) => {
            Posts.enableEdit(event.target)//Pega o elemento que desparou esse evento
        })

        $(btnSave).on('click', (event) => {
            Posts.update(event.target)
        })

        $(btnRemove).on('click', (event) => {
            Posts.remove(event.target)//Pega o elemento que desparou esse evento
        })

        
        $(user).append(date)
        $(comment).append(content)
        $(comment).append(user)
        $(comment).append(btnEdit)
        $(comment).append(btnSave)
        $(comment).append(btnRemove)
        $("#comments").append(comment)
    },

    findAll : () => {
        
        $.ajax({
            type: "GET",
            url: "/post",
            success : (data) => {
                //Aqui recebe os dados
                //Vetor com vários posts
                for(var post of data){
                    Posts.template(post)
                }
            },
            erro : () => {
                console.log("Ocorreu um erro")
            },
            dataType: "json"

        })
    },

    enableEdit : (button) => {
        var comment = $(button).parent()

        $(comment).children('textarea').prop("disabled", false)
        $(comment).children('button.edit').hide()
        $(comment).children('button.save').show()

    }, 

    update : (button) => {
        var comment = $(button).parent()

        var id = $(comment).attr('id').replace('comment-', '')
        var content = $(comment).children('textarea').val()

        $.ajax({
            type: "PUT",
            url: "/post",
            data:{'content' : content, 'id' : id},
            success : (data) => {
                //Quando der certo
                $(comment).children('textarea').prop("disabled", true)
                $(comment).children('button.edit').show()
                $(comment).children('button.save').hide()
            },
            erro : () => {
                console.log("Ocorreu um erro")
            },
            dataType: "json"

        })
    },

    remove: (button) => {
        var comment = $(button).parent()
        var id = $(comment).attr('id').replace('comment-', '')

        $.ajax({
            type: "DELETE",
            url: "/post",
            data:{'id' : id},
            success : (data) => {
                $(comment).remove()
            },
            erro : () => {
                console.log("Ocorreu um erro")
            },
            dataType: "json"
        })
    }, 
    
    Campo: () =>  {
        let  comentários = $('#comments')[0]
        
        while(comentários.firstChild )  {
            comentários.removeChild(comentários.firstChild) ;
        }
    },

    buscaComentarios: ()  =>  {
        var areaSearch = $("#buscaTexto").val() ;

        $.ajax ({
            type : 'GET',
            url : '/posts/search',
            dataType : 'json',
            data : {'content': areaSearch},
            success: (data) => {
                Posts.Campo();
                data.forEach (element => Posts.template(element));
            },
            error : ()  =>  {
              console.log ('Ocorreu um erro!');
            }
        })
    },

    limpaUsuario: () => {
        var usuariosCadastrados = $("#usuarios")
        while(usuariosCadastrados){
            console.log("\n\n Exluindo \n\n")
            usuariosCadastrados.removeChild(usuariosCadastrados.firstChild) ;
        }
    },

    buscaUsuarios: () => {
        $.ajax({
            type: "GET",
            url: "/posts/users",
            dataType: "json",
            success : (data) => {
                console.log("\n\n\n DENTRO DE BUSCA USUARIOS\n\n\n")
                Posts.limpaUsuario
                var usuariosCadastrados = $("#usuarios")
                for(var user of data){
                    var option = $('<option></option>').html(user.firstname)
                    usuariosCadastrados.append(option)
                }
            },
            erro : () => {
                console.log("Ocorreu um erro")
            },
        })
    },
    /*acharIdUsuario: () => {
        $.ajax({
            type: "GET",
            url: "/posts/usuarioId",
            success : (data) => {
                res.send(data)
            },
            erro : () => {
                console.log("Ocorreu um erro")
            },
            dataType: "json"

        })
    }*/
    
    cadastrarUsuario: () => {
        var t = {}
        t.firstname = $("#firstname").val()
        t.lastname = $("#lastname").val()

        //console.log("Nome.......: " + t.firstname)
        //console.log("Sobrenome..: " + t.lastname)
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/posts/cadastro",
            data: t,
            success: Posts.buscaUsuarios
        })
    }
}

//Funcao ready, recebe um callback
//Quando a página acabra de carregar essa função será executada
$(document).ready(()=>{
    Posts.findAll()
    Posts.buscaUsuarios
})